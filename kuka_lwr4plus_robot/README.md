# FRI ROS2

This is a package for connecting to a KUKA LWR 4+ robotic arm. Right now it is
implemented and tested for working with ROS2-humble

## Instructions for developers

### High priority permission

This node will need to set a thread with very high priority. In order to give
permission to your user to do it you must do the following.

- Install the **PREEMPT_RT** kernel
- Create a group and add your user to it

```
sudo addgroup realtime
    sudo usermod -a -G realtime $(whoami)
```

- Afterwards edit `/etc/security/limits.conf`, add the following

```
@realtime soft rtprio 99
@realtime soft priority 99
@realtime soft memlock 102400
@realtime hard rtprio 99
@realtime hard priority 99
@realtime hard memlock 102400
```

### Building the node
As the project progresses we are going to need different things. For now we need
xstow to be able to install the FRI-stanford library.

Install and configure **xstow** following [lab
instructions](https://wiki.arcoslab.org/doku.php?id=tutorials:using_xstow_for_local_installations)

Now that you have xstow, clone the `fri-stanford` library into `~/local/src`,
and compile, and install it. The following comamnds will leave the required fri
files for static linkage that we need for our node.

```.bash
cd ~/local/src
git clone git@git.arcoslab.org:humanoid-software/fri-stanford.git
cd Linux
make all_release_x64
bash install.sh
cd ~/local/DIR
xstow fri
```

Go to the ROS2 workspace and run

```.bash
colcon build --symlink-install
```

Then make sure to source the local `setup.bash`. To have all the messages
available to ROS2.

### Running

Open a terminal as root and source the ROS2 setup file

```.bash
source /opt/ros/dashing/setup.bash
```

Then source the setup file at tour local workspace

```.bash
source <user-home>/<workspace>/install/setup.bash
```

Now, start the KUKA LWR4+ arm and get it ready for FRI communication. You can
follow this
[wiki](https://wiki.arcoslab.org/doku.php?id=tutorials:object_manipulation_robot_simulator&s[]=stanford#fri_robot_communication)
for instructions. *Notice: Only follow the part for running the robot, that deals with
the KRC, the computer configuration is different*. Remember to configure the
ethernet port that is directly connected to the KRC to use the IP `192.168.2.113`.


Once the arm is ready, you can execute the node (in the terminal where you are
logged in as root).

```.bash
ros2 run fri_ros2 fri_node
```

To make the robot move, you can execute the keyboard controller. In another
terminal source the ROS2 setup file and your local workspace setup file. Then
run

```.bash
ros2 run fri_tester fri_keyboard
```

To stop the node, just cancel it with `Ctrl+C`. The node will handle the signal
and gracefully shutdown the connection with the arm.
