// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <iostream>
#include <mutex>

#include <LWRJointImpedanceController.h>

#include <common_defines.hpp>

#pragma once

//! Measured data from robot (obtained within the last control cycle)
class RobotData {
public:
  /// Constructor of the data container
  RobotData();
  /// Copies the data from the source to self. This operation is thread safe
  /// and the correct way to get data from any RobotData.
  ///
  /// \param src the source from which data is copied
  void copy_from(RobotData *src);
  float commanded[NUMBER_OF_JOINTS];  //!< commanded joint angle [rad]
  float position[NUMBER_OF_JOINTS]; //!< current joint angle [rad]
  float torque[NUMBER_OF_JOINTS]; //!< current joint torque [Nm]
  float torqueTCP[12]; //!< estimated torque at TCP [Nm]
  float temperature[NUMBER_OF_JOINTS];  //!< Joint temperature [C]
  std::mutex locker; //!< Mutex for thread safety
};
