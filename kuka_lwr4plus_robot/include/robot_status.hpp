// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <iostream>
#include <mutex>

#include <LWRJointImpedanceController.h>

#include <common_defines.hpp>

#pragma once

//! General status of the robot (measured within the last control cycle)
class RobotStatus {
public:
  /// COnstructor of the robot status container
  RobotStatus();
  /// Copies data from source to self. This operation is thread safe.
  ///
  /// \param src the source from which data is copied
  void copy_from(RobotStatus *src);
  /// Get the current status from the robot
  ///
  /// \param robot pointer to the FRI structure that interfaces with the robot
  void from_robot(FastResearchInterface* robot);
  /// Set the control frequencies by the Main FRI thread
  void set_frequencies(double step_duration, double fri_frequency);
  /// Read the step duration by the ROS2 thread
  double get_step_duration();
  /// Read the FRI frequency control loop by the ROS2 thread
  double get_fri_frequency();
  bool hardware_enabled; //!< KRC reports hardware is enabled
  bool software_enabled; //!< ROS2 node software enable
  bool drives_warning; //!< If any joint reports a warning
  int fri_mode;  //!< off=0, monitor=1, command=2
  int control_scheme; //!< joint position, cart impedance, joint impedance, joint torque
  int communication_timing_quality; //!< Unacceptable, bad, ok, perfect
  std::mutex locker; //!< Mutex for thread safety
private:
  double control_loop_step_duration; //!< Duration of the control loop step (average last 100)
  double fri_frequency; //!< Frequency of the FRI interface control loop (average last 100)
};
