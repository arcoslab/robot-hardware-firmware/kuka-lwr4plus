// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


//Ruckig
#include <ruckig/ruckig.hpp>

// general
#include <TypeIRML.h>
#include <errno.h>
#include <iostream>
#include <math.h>
#include <signal.h> // SIGINT signal handling
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>
#include <LWRJointImpedanceController.h>
#include <deque>
#include <vector>
#include <array>

// ROS
#include <rclcpp/clock.hpp>
#include <rclcpp/time.hpp>

// Internal headers
#include <common_defines.hpp>
#include <robot_data.hpp>
#include <robot_cmd.hpp>
#include <robot_status.hpp>
#include <safety.hpp>


#pragma once


/// Handler of the Fast Research Interface with KUKA. This internal interface will run in its
/// own thread. This is done to make sure that we can operate within the time constraints that
/// the KUKA arm needs.
class FriInterface {
public:
  /// Constructor of the interface handler.
  ///
  /// \param in_data robot state container
  /// \param in_cmd commands to sned to the robot container
  /// \param in_status current robot status container
  /// \param server_port_number port to use for FRI UDP connection
  /// \param server_ip_address big endian representation of the host ip address
  FriInterface(RobotData* in_data,
               RobotCommand* in_cmd,
               RobotStatus* in_status,
               int server_port_number,
               unsigned int server_ip_address,
               std::vector<double> max_velocity,
               std::vector<double> max_acceleration,
               std::vector<double> max_jerk);
  /// starts a thread that communicates with the robot
  void start();
  /// The actual control loop that executes communication with robot
  void control_loop();
  /// Forces stop of the interface
  void stop();
private:
  /// Control step that is executed in each loop
  int step();

  FastResearchInterface* Robot;

  bool keep_looping;
  bool connected;

  TypeIRML *RML;
  TypeIRMLInputParameters *IP;
  TypeIRMLOutputParameters *OP;

  RobotData* external_data;
  RobotData data;
  RobotCommand* external_cmd;
  RobotCommand cmd;
  RobotStatus* robot_status;

  const double liml[NUMBER_OF_JOINTS] = {-169.5 DEGS, -119.5 DEGS, -169.5 DEGS, -119.5 DEGS, -169.5 DEGS, -119.5 DEGS, -169.5 DEGS};
  const double limh[NUMBER_OF_JOINTS] = { 169.5 DEGS,  119.5 DEGS,  169.5 DEGS,  119.5 DEGS,  169.5 DEGS,  119.5 DEGS,  169.5 DEGS};

  ruckig::Ruckig<NUMBER_OF_JOINTS> my_ruckig {0.001};
  ruckig::InputParameter<NUMBER_OF_JOINTS> ruckig_input;
  ruckig::OutputParameter<NUMBER_OF_JOINTS> ruckig_output;
  ruckig::Result result;

  // time control
  rclcpp::Time prev_time;
  rclcpp::Time time_zero;
  std::deque<double> step_durations;
  std::deque<double> fri_frequencies;
  rclcpp::Clock internal_clock;


};

/// Function that is used to execute the interface in a separate thread
///
/// \param fri_interface the actual interface that is being used
void loop(FriInterface* fri_interface);
