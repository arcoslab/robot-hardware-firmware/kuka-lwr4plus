// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <math.h>

#ifndef NUMBER_OF_JOINTS
#define NUMBER_OF_JOINTS 7
#endif

#ifndef DEG_TO_RAD
#define DEG_TO_RAD(A) ((A)*M_PI / 180.0)
#endif

#define DEGS *M_PI / 180.0

#define MAX_SPEED DEG_TO_RAD(90)

#define RUN_TIME_IN_SECONDS 10.0

#define SAFETY_LEFT 0
#define SAFETY_RIGHT 1

#define CYCLE_PERIOD 0.001

#define SERVER_PORT_NUMBER 49938

#define ROBOT_FRI_DRIVER_NAME "980039"

// Priorities go from 1 to 63
#define KRC_COMMUNICATION_THREAD_PRIORITY 60
#define TIMER_THREAD_PRIORITY 55
#define MAIN_THREAD_PRIORITY 50
#define OUTPUT_CONSOLE_THREAD_PRIORITY 5

#define NUMBER_OF_LOGGING_FILE_ENTRIES 60000
#ifndef LOGGING_PATH
#define LOGGING_PATH "/home/demo/.ros/log"
#endif
#define LOGGING_FILE_NAME "LWR-Scope.dat"
