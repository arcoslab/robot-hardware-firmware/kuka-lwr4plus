// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <robot_data.hpp>
#include <robot_cmd.hpp>

// interpolate the value of y at point x, given the points (x1, y1) and (x2, y2)
double interpolate(double x, double x1, double y1, double x2, double y2);

// TODO: add docs
int find_index(float j5);

// TODO: add docs
double min_j6(float j5, int index);

//TODO: add docs
double max_j6(float j5, int index);

// TODO: add docs
void safety_check(float *vel, float *vel_old, float *pos, float rate);

// TODO: add docs
int safety_set_side(int side);

// TODO: add_docs
void clip_speed(RobotCommand *cmd, RobotData *data);
