// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <mutex>
#include <string>

#include <common_defines.hpp>

#pragma once

/// Command data for robot (to be executed within the next control sycle)
class RobotCommand {
public:
  /// Constructor of the command data container
  RobotCommand();
  /// Copies the data from the source to self. This operation is thread safe
  /// and the correct way to get the data from any RobotCommand.
  ///
  /// \param src the source from which data is copied.
  void copy_from(RobotCommand *src);
  float command[NUMBER_OF_JOINTS]; //!< desired joint angle [rad]
  float stiffness[NUMBER_OF_JOINTS]; //!< stiffness [Nm/rad]
  float damping[NUMBER_OF_JOINTS];   //!< damping [Nm*s/rad]
  float addTorque[NUMBER_OF_JOINTS]; //!< additional torque [Nm]
  float velocity[NUMBER_OF_JOINTS]; //!< desired joint angle velocity [rad/s]
  std::mutex locker; //!< Mutex for thread safety
  /// Transforms the command data into a string. Warning! this operation
  /// is not thread safe.
  std::string to_str();
  /// Print some of the info in the command. Warning! this operation
  /// is not thread safe.
  void print();
};
