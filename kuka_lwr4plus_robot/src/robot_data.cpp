// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <robot_data.hpp>


RobotData::RobotData() {
  for (int i = 0; i < NUMBER_OF_JOINTS; i++) {
    commanded[i] = 0.0;
    position[i] = 0.0;
    torque[i] = 0.0;
    temperature[i] = 25.0;
  }
  for (int i=0; i<12; i++) torqueTCP[i]=0.0;
}

void RobotData::copy_from(RobotData *src){
  const std::lock_guard<std::mutex> lock_from(src->locker);
  const std::lock_guard<std::mutex> lock_to(this->locker);
  for (int i=0; i<NUMBER_OF_JOINTS; i++){
    this->commanded[i] = src->commanded[i];
    this->position[i] = src->position[i];
    this->torque[i] = src->torque[i];
    this->temperature[i] = src->temperature[i];
  }
}
