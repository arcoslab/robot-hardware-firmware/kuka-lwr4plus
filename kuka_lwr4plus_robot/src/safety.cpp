// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <common_defines.hpp>
#include <robot_data.hpp>
#include <robot_cmd.hpp>
#include <safety.hpp>


int should_exit = 0;

// general limits (applying to both arms)
double liml2[7] = {-169.5, -119.5, -169.5, -119.5, -169.5, -119.5, -169.5};
double limh2[7] = {169.5, 119.5, 169.5, 119.5, 169.5, 119.5, 169.5};

double j5_angles_left[] = {-120.0 DEGS, -100 DEGS, -80 DEGS, -60 DEGS, -40 DEGS, -35 DEGS, -30 DEGS,
                           -25 DEGS,    -20 DEGS,  -10 DEGS, 0 DEGS,   10 DEGS,  20 DEGS,  30 DEGS,
                           40 DEGS,     50 DEGS,   60 DEGS,  70 DEGS,  80 DEGS,  100 DEGS, 120 DEGS};
double j6_min_left[] = {-170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS,
                        -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -160 DEGS, -150 DEGS, -140 DEGS,
                        -105 DEGS, -90 DEGS,  -85 DEGS,  -85 DEGS,  -80 DEGS,  -75 DEGS,  -75 DEGS};
double j6_max_left[] = {
                        -4 DEGS, -4 DEGS, 0 DEGS,  0 DEGS,  10 DEGS, 30 DEGS,
                        50 DEGS, 60 DEGS, 65 DEGS, 70 DEGS, 80 DEGS, 80 DEGS,
                        85 DEGS, 90 DEGS, 90 DEGS, 90 DEGS, 90 DEGS, 90 DEGS,
                        90 DEGS, 90 DEGS, 90 DEGS};
int length_left = 21;

double j5_angles_right[]={-120 DEGS, -100 DEGS, -80 DEGS, -60 DEGS, -40 DEGS,
			  -20 DEGS,  -17 DEGS,  -15 DEGS, -10 DEGS, 0 DEGS,
			  20 DEGS,   30 DEGS,   40 DEGS,  60 DEGS,  80  DEGS,
			  100 DEGS, 120 DEGS};
double j6_min_right[]={-170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS,
		       -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS, -170 DEGS,
		       -170 DEGS, -140 DEGS, -105 DEGS, -95 DEGS,  -90 DEGS,
		       -90 DEGS,  -90 DEGS};
double j6_max_right[]={-5 DEGS, -10 DEGS, -5 DEGS,  0 DEGS,   15 DEGS,  70 DEGS,
		       70 DEGS, 170 DEGS, 170 DEGS, 170 DEGS, 170 DEGS, 130 DEGS,
		       130 DEGS, 130 DEGS, 130 DEGS, 130 DEGS, 110 DEGS};
int length_right = 17;

int length = 0;

double *j5_angles = 0;
double *j6_min = 0;
double *j6_max = 0;

// general limits (applying to both arms)
double liml[7] = {-169.5 DEGS, -119.5 DEGS, -169.5 DEGS, -119.5 DEGS,
                  -169.5 DEGS, -119.5 DEGS, -169.5 DEGS};
double limh[7] = {169.5 DEGS, 119.5 DEGS, 169.5 DEGS, 119.5 DEGS,
                  169.5 DEGS, 119.5 DEGS, 169.5 DEGS};


double interpolate(double x, double x1, double y1, double x2, double y2) {
  double diff_y = y2 - y1;
  double diff_x = x2 - x1;
  double dx = x - x1;
  return (y1 + (diff_y / diff_x) * dx);
}

int find_index(float j5) {
  // printf("Array length: %d\n",length);

  int i = 0;

  // check for errors
  if ((j5 < j5_angles[0]) || (j5 > j5_angles[length - 1])) {
    printf("%f not in [%f .. %f]: ", j5, j5_angles[0], j5_angles[length - 1]);
    printf("Error, check the input value.\n");
    return (-1);
  }

  // The array must increase monotically
  // need the index where we interpolate
  int index = 0;
  for (i = 0; i <= length - 1; i++) {
    if ((j5 > j5_angles[i]) & (j5 <= j5_angles[i + 1])) {
      index = i;
      break;
    }
  }

  return (index);
}

double min_j6(float j5, int index) {
  float min_lim = interpolate(j5, j5_angles[index], j6_min[index],
                              j5_angles[index + 1], j6_min[index + 1]);
  return (min_lim);
}

double max_j6(float j5, int index) {
  float max_lim = interpolate(j5, j5_angles[index], j6_max[index],
                              j5_angles[index + 1], j6_max[index + 1]);
  return (max_lim);
}

void safety_check(float *vel, float *vel_old, float *pos, float rate) {
  if (length == 0 || j5_angles == 0 || j6_min == 0 || j6_max == 0) {
    printf("arm side for safety limits checking not set, panicking...\n");
    std::exit(-1);
  }

  // The following values vel_max and acc_max are decided by us
  float vel_max = (100 DEGS) * rate;
  float acc_max = (600 DEGS) * rate * rate;

  // These are the hard acceleration limits defined in the RSI-Def7 file
  float acc_max_hard[7];
  acc_max_hard[0] = (1200 DEGS) * rate * rate;
  acc_max_hard[1] = (1200 DEGS) * rate * rate;
  acc_max_hard[2] = (1600 DEGS) * rate * rate;
  acc_max_hard[3] = (1600 DEGS) * rate * rate;
  acc_max_hard[4] = (2500 DEGS) * rate * rate;
  acc_max_hard[5] = (4400 DEGS) * rate * rate;
  acc_max_hard[6] = (4400 DEGS) * rate * rate;

  // rate down the accel limits for safety
  for (unsigned int i = 0; i < 7; i++) {
    acc_max_hard[i] = acc_max_hard[i] * 0.9;
  }

  // do a special check for the last two joints, they need to use the 2d map of
  // their limits check if we are inside the nice area, if not, stop!
  float newpos5 = pos[5] + vel[5];
  int index = find_index(newpos5);
  float min_lim6 = min_j6(newpos5, index);
  float max_lim6 = max_j6(newpos5, index);
  int n_cycles = 5;

  // lower limit, speed negative
  if ((pos[6] + vel[6] < min_lim6) && (pos[6] + n_cycles * vel[6] < min_lim6)) {
    vel[6] = (0.0 > vel_old[6] + acc_max) ? vel_old[6] + acc_max : 0.0;
    // limit 5 also (both sides)
    vel[5] = (0.0 > vel_old[5] + acc_max) ? vel_old[5] + acc_max : 0.0;
    vel[5] = (0.0 < vel_old[5] - acc_max) ? vel_old[5] - acc_max : 0.0;
  }

  // upper limit, speed positive
  if ((pos[6] + vel[6] > max_lim6) && (pos[6] + n_cycles * vel[6] > max_lim6)) {
    vel[6] = (0.0 < vel_old[6] - acc_max) ? vel_old[6] - acc_max : 0.0;
    // limit 5 also (both sides)
    vel[5] = (0.0 > vel_old[5] + acc_max) ? vel_old[5] + acc_max : 0.0;
    vel[5] = (0.0 < vel_old[5] - acc_max) ? vel_old[5] - acc_max : 0.0;
  }

  // final check on all the joints, avoiding to exceed their mechanical limits
  for (int i = 0; i < 7; i++) {

    // handle velocity limits
    vel[i] = (vel[i] > vel_max) ? vel_max : vel[i];
    vel[i] = (vel[i] < -vel_max) ? -vel_max : vel[i];

    // handle normal acceleration limits
    vel[i] = (vel[i] > vel_old[i] + acc_max) ? vel_old[i] + acc_max : vel[i];
    vel[i] = (vel[i] < vel_old[i] - acc_max) ? vel_old[i] - acc_max : vel[i];

    // Limit handling in two stages:
    // Stage 1:
    // calculate the maximum speed now,
    // so that we could still stop in time for the limit, i.e.
    // | v(t) = a_max*t,  x(t) = Int(v(t)*dt) = .5*a_max*t^2
    // | ==> v(x) = sqrt(2*a_max*x)
    // | x: distance to limit
    // | v: velocity
    // | a_max: maximum allowed accelearation

    // BUT: We need to know the velocity that we mustn't exceed
    //      at the end of the NEXT timestep:
    // | [ x_next = x - v
    // | [ v = sqrt(2*a_max*x_next)
    // | ==> v(x) = -a + sqrt(a_max^2 + 2*a_max*x)
    if (vel[i] > 0.0) { // positive speed, worry about upper limit
      float dist_to_limit = limh[i] - pos[i];
      if (dist_to_limit > 0.0) { // only change vel[i] if we are before the
                                 // limit
        vel_max = -acc_max_hard[i] +
                  sqrt(acc_max_hard[i] * (2 * dist_to_limit + acc_max_hard[i]));

        vel[i] = (vel[i] > vel_max) ? vel_max : vel[i];
      }
    } else { // negative speed, worry about lower limit
      float dist_to_limit = pos[i] - liml[i];
      if (dist_to_limit > 0.0) {
        vel_max = -acc_max_hard[i] +
                  sqrt(acc_max_hard[i] * (2 * dist_to_limit + acc_max_hard[i]));

        vel[i] = (vel[i] < -vel_max) ? -vel_max : vel[i];
      }
    }

    // Stage 2:
    // emergency braking taking advantage of the limit margin (0.5deg)
    if (((pos[i]) < liml[i]) &&
        (vel[i] < 0.0)) { // negative velocity, hitting lower limit
      vel[i] = (0.0 > vel_old[i] + acc_max_hard[i])
                   ? vel_old[i] + acc_max_hard[i]
                   : 0.0;
    }
    if (((pos[i]) > limh[i]) &&
        (vel[i] > 0.0)) { // positive velocity, hitting upper limit
      vel[i] = (0.0 < vel_old[i] - acc_max_hard[i])
                   ? vel_old[i] - acc_max_hard[i]
                   : 0.0;
    }
  }
}

int safety_set_side(int side) {
  switch (side) {
  case SAFETY_LEFT:
    j5_angles = j5_angles_left;
    j6_min = j6_min_left;
    j6_max = j6_max_left;
    length = length_left;
    return 1;
  case SAFETY_RIGHT:
    j5_angles = j5_angles_right;
    j6_min = j6_min_right;
    j6_max = j6_max_right;
    length = length_right;
    return 1;
  default:
    return 0;
  }
}

void clip_speed(RobotCommand *cmd, RobotData *data) {
  float max_clip = 1.0;
  float tmp_clip;
  for (int j = 0; j < NUMBER_OF_JOINTS; j++) {
    if (cmd->command[j] > MAX_SPEED) {
      tmp_clip = cmd->command[j] / MAX_SPEED;
      if (tmp_clip > max_clip) {
        max_clip = tmp_clip;
      }
    }
    if (cmd->command[j] < -MAX_SPEED) {
      tmp_clip = -cmd->command[j] / MAX_SPEED;
      if (tmp_clip > max_clip) {
        max_clip = tmp_clip;
      }
    }
  }
  for (int j = 0; j < NUMBER_OF_JOINTS; j++) {
    if (max_clip > 1.0) {
      printf("Cliping: ");
      printf(" %f", cmd->command[j]);
      cmd->command[j] = cmd->command[j] / max_clip;
      printf(" %f,", cmd->command[j]);
      if (j == 6) {
        printf("\n");
      }
    }
  }
  for (int j = 0; j < NUMBER_OF_JOINTS; j++) {
    if (((data->position[j] <= DEG_TO_RAD(liml2[j])) && (cmd->command[j] < 0.0)) ||
        ((data->position[j] >= DEG_TO_RAD(limh2[j])) && (cmd->command[j] > 0.0))) {
      printf("Joint limit axis: %d!\n", j);
      cmd->command[j] = 0.0;
    }
  }
}
