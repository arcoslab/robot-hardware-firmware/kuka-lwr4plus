// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <robot_cmd.hpp>


RobotCommand::RobotCommand() {
  for (int i = 0; i < NUMBER_OF_JOINTS; i++) {
    command[i] = 0.0;
    stiffness[i] = 100.0;
    damping[i] = 0.7;
    addTorque[i] = 0.0;
    velocity[i] = 0.0;
  }
}

void RobotCommand::copy_from(RobotCommand *src){
  const std::lock_guard<std::mutex> lock_from(src->locker);
  const std::lock_guard<std::mutex> lock_to(this->locker);
  for (int i=0; i<NUMBER_OF_JOINTS; i++){
    this->command[i] = src->command[i];
    this->stiffness[i] = src->stiffness[i];
    this->damping[i] = src->damping[i];
    this->addTorque[i] = src->addTorque[i];
    this->velocity[i] = src->velocity[i];
  }
}

std::string RobotCommand::to_str(){
  std::string str = "Robot command:\n";
  for (int i=0; i<NUMBER_OF_JOINTS; i++){
    str + std::to_string(command[i]) + " ";
  }
  str + "\n";
  return str;

}


void RobotCommand::print(){
  std::cout<<"-----------------------"<<std::endl;
  std::cout<<"Robot velocity: "<<std::endl;
  for (int i=0; i<NUMBER_OF_JOINTS; i++){
    std::cout<<velocity[i]<<" ";
  }
  std::cout<<std::endl<<"Robot Position: "<<std::endl;
  for (int i=0; i<NUMBER_OF_JOINTS; i++){
    std::cout<<command[i]<<" ";
  }
  std::cout<<std::endl;
}
