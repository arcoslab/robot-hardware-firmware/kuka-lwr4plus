// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.



#include <thread>
#include <mutex>
#include <errno.h>
#include <signal.h> // SIGINT signal handling
#include <stdio.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <LWRJointImpedanceController.h>

// ROS includes
#include <rclcpp/rclcpp.hpp>
#include "std_msgs/msg/string.hpp"
#include "impedance_control_msgs/msg/state.hpp" // The new <fri_msgs/msg/kuka_state.hpp>
#include "impedance_control_msgs/msg/pos_ref.hpp" // The new <fri_msgs/msg/kuka_cmd.hpp>
#include "impedance_control_msgs/msg/rt_params.hpp" // The new <fri_msgs/msg/kuka_config.hpp>
#include "impedance_control_msgs/msg/status.hpp"
#include "impedance_control_msgs/srv/modes.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "kuka_lwr4plus_msgs/msg/kuka_debug.hpp"

// Internal headers
#include <common_defines.hpp>
#include <fri_interface.hpp>

#define DEFAULT_MAX_VEL 1.0
#define DEFAULT_MAX_ACCEL 2.0
#define DEFAULT_MAX_JERK 50.0

using namespace std;
using std::placeholders::_1;
using std::placeholders::_2;


FriInterface* fri_interface;

/*struct ThreadInput{
  RobotData* data;
  RobotCommand* cmd;
  };*/

class FriNode : public rclcpp::Node{
public:
  FriNode(RobotData* in_data, // Robot current state
          RobotCommand* in_cmd, // Robot current command
          RobotStatus* in_status
          ) : Node("lwr4"){
    this->configure_correct = true;
    enabled = false;
    hardware_enabled = true;
    data = in_data;
    cmd = in_cmd;
    robot_status = in_status;
#ifdef MEASURE_TIME
    counter = 0;
    prev_time = this->now().seconds();
#endif
    //ROS2 parameters
    this->declare_parameter("robot_name", "r_klwr4p");
    this->declare_parameter("frame_id_basename_suffix", "_base");
    std::vector<double> default_max_vel = {
      DEFAULT_MAX_VEL, // 0
      DEFAULT_MAX_VEL, // 1
      DEFAULT_MAX_VEL, // 2
      DEFAULT_MAX_VEL, // 3
      DEFAULT_MAX_VEL, // 4
      DEFAULT_MAX_VEL, // 5
      DEFAULT_MAX_VEL // 6
    };
    std::vector<double> default_max_accel = {
      DEFAULT_MAX_ACCEL, // 0
      DEFAULT_MAX_ACCEL, // 1
      DEFAULT_MAX_ACCEL, // 2
      DEFAULT_MAX_ACCEL, // 3
      DEFAULT_MAX_ACCEL, // 4
      DEFAULT_MAX_ACCEL, // 5
      DEFAULT_MAX_ACCEL // 6
    };
    std::vector<double> default_max_jerk = {
      DEFAULT_MAX_JERK, // 0
      DEFAULT_MAX_JERK, // 1
      DEFAULT_MAX_JERK, // 2
      DEFAULT_MAX_JERK, // 3
      DEFAULT_MAX_JERK, // 4
      DEFAULT_MAX_JERK, // 5
      DEFAULT_MAX_JERK, // 6
    };
    this->declare_parameter("max_velocity", default_max_vel);
    this->declare_parameter("max_acceleration", default_max_accel);
    this->declare_parameter("max_jerk", default_max_jerk);
    this->max_velocity = this->get_parameter("max_velocity").as_double_array();
    this->max_acceleration = this->get_parameter("max_acceleration").as_double_array();
    this->max_jerk = this->get_parameter("max_jerk").as_double_array();

    if (this->max_velocity.size() != NUMBER_OF_JOINTS) {
      this->configure_correct = false;
      RCLCPP_ERROR(this->get_logger(), "Wrong number of joints in max_velocity param");
    }
    if (this->max_acceleration.size() != NUMBER_OF_JOINTS) {
      this->configure_correct = false;
      RCLCPP_ERROR(this->get_logger(), "Wrong number of joints in max_acceleration param");
    }
    if (this->max_jerk.size() != NUMBER_OF_JOINTS) {
      this->configure_correct = false;
      RCLCPP_ERROR(this->get_logger(), "Wrong number of joints in max_jerk param");
    }

    // Get the IP address of the server
    this->declare_parameter("host_ip_address", "255.255.255.255");
    auto host_ip = this->get_parameter("host_ip_address").as_string();
    struct in_addr addr;
    if (inet_aton(host_ip.c_str(), &addr) != 0) {
      this->server_ip_address =  __builtin_bswap32(addr.s_addr);
    } else {
      this->configure_correct = false;
      RCLCPP_ERROR(this->get_logger(), "Could not convert IP address");
    }

    // Enable/disable service
    srv_modes = this->create_service<impedance_control_msgs::srv::Modes>(
      std::string(this->get_name())+"/control", std::bind(&FriNode::modes_callback, this, _1, _2));

    //state of the robot
    state_pub = this->create_publisher<impedance_control_msgs::msg::State>(
      std::string(this->get_name())+"/state", 10);

    //"joint_states" necessary for ROS2 TF
    joint_states_pub = this->create_publisher<sensor_msgs::msg::JointState>(
      std::string(this->get_name())+"/joint_states", 10);

    //status of the robot
    status_pub = this->create_publisher<impedance_control_msgs::msg::Status>(
      std::string(this->get_name())+"/status", 10);

    //debug of the robot
    kuka_debug_pub = this->create_publisher<kuka_lwr4plus_msgs::msg::KukaDebug>(
      std::string(this->get_name())+"/kuka_debug", 10);


    // Get the joint position commands
    cmd_sub = this->create_subscription<impedance_control_msgs::msg::PosRef>(
      std::string(this->get_name())+"/posref", 10, std::bind(&FriNode::cmd_callback, this, _1));

    // Get the Real time parameters commands
    config_sub = this->create_subscription<impedance_control_msgs::msg::RTParams>(
      std::string(this->get_name())+"/rtparams", 10, std::bind(&FriNode::config_callback, this, _1));

    // Timers
    timer = this->create_wall_timer(1ms, std::bind(&FriNode::timer_callback, this));
    joint_state_timer = this->create_wall_timer(10ms, std::bind(&FriNode::joint_state_timer_callback, this));
    status_timer = this->create_wall_timer(200ms, std::bind(&FriNode::status_callback, this));
    RCLCPP_INFO(this->get_logger(), "FRI node is ready for ROS communication");
  }

  int get_server_ip(void){
    return this->server_ip_address;
  }

  std::vector<double> max_velocity;
  std::vector<double> max_acceleration;
  std::vector<double> max_jerk;
  bool configure_correct;

private:
  bool enabled;
  bool hardware_enabled;
  unsigned int server_ip_address;
#ifdef MEASURE_TIME
  int counter;
  double prev_time;
  double frequencies[100];
#endif
  rclcpp::TimerBase::SharedPtr timer;
  rclcpp::TimerBase::SharedPtr joint_state_timer;
  rclcpp::TimerBase::SharedPtr status_timer;
  rclcpp::Publisher<impedance_control_msgs::msg::State>::SharedPtr state_pub;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr joint_states_pub;
  rclcpp::Publisher<impedance_control_msgs::msg::Status>::SharedPtr status_pub;
  rclcpp::Publisher<kuka_lwr4plus_msgs::msg::KukaDebug>::SharedPtr kuka_debug_pub;
  rclcpp::Subscription<impedance_control_msgs::msg::PosRef>::SharedPtr cmd_sub;
  rclcpp::Subscription<impedance_control_msgs::msg::RTParams>::SharedPtr config_sub;
  rclcpp::Service<impedance_control_msgs::srv::Modes>::SharedPtr srv_modes;
  RobotData* data;
  RobotCommand* cmd;
  RobotStatus* robot_status;

  void timer_callback(){
    RCLCPP_DEBUG(this->get_logger(), "timer start");
    auto message = impedance_control_msgs::msg::State();
    {
      const std::lock_guard<std::mutex> lock_status(robot_status->locker);
      const std::lock_guard<std::mutex> lock_state(data->locker);
      robot_status->software_enabled = enabled;
      hardware_enabled = robot_status->hardware_enabled;
      message.position = std::vector<float>(begin(data->position), end(data->position));
      message.cmdedposition = std::vector<float>(begin(data->commanded), end(data->commanded));
      message.effort = std::vector<float>(begin(data->torque), end(data->torque));
    }
    state_pub->publish(message);
    RCLCPP_DEBUG(this->get_logger(), "timer end");
  }
  void joint_state_timer_callback(){
    RCLCPP_DEBUG(this->get_logger(), "Started cmd callback");
    // Create two joint state messages, one with the sensed data and one with the
    // commanded data
    auto message = sensor_msgs::msg::JointState();
    auto cmd_msg = sensor_msgs::msg::JointState();
    message.header.stamp = this->now();
    cmd_msg.header.stamp = message.header.stamp;
    message.name = std::vector<string> {};
    cmd_msg.name = std::vector<string> {};

    message.velocity = std::vector<double> {};
    cmd_msg.velocity = std::vector<double> {};

    for (int i=0; i<NUMBER_OF_JOINTS; i++){
      message.velocity.push_back(0.0);
      cmd_msg.velocity.push_back(0.0);
    }
    {
      const std::lock_guard<std::mutex> lock_data(data->locker);
      message.effort = std::vector<double>(begin(data->torque), end(data->torque));
      cmd_msg.effort = std::vector<double>(begin(data->torque), end(data->torque));
      message.position = std::vector<double>(begin(data->position), end(data->position));
      cmd_msg.position = std::vector<double>(begin(data->commanded), end(data->commanded));
    }
    // Up to this point both messages are the same.
    // The differences are the base frame, the joint names and the position data
    message.header.frame_id = this->get_parameter("robot_name").as_string()+this->get_parameter("frame_id_basename_suffix").as_string();
    cmd_msg.header.frame_id = "cmd_" + message.header.frame_id;

    for (int i=0; i<NUMBER_OF_JOINTS; i++){
      message.name.push_back(this->get_parameter("robot_name").as_string()+"_j"+std::to_string(i));
      cmd_msg.name.push_back("cmd_"+this->get_parameter("robot_name").as_string()+"_j"+std::to_string(i));
    }

    // Publish both messages
    joint_states_pub->publish(message);
    joint_states_pub->publish(cmd_msg);
    RCLCPP_DEBUG(this->get_logger(), "Finished joint state callback");
  }
  void status_callback(){
    RCLCPP_DEBUG(this->get_logger(), "Started status callback");
    auto status_msg = impedance_control_msgs::msg::Status();
    auto kuka_debug_msg = kuka_lwr4plus_msgs::msg::KukaDebug();
    {
      const std::lock_guard<std::mutex> lock(data->locker);
      status_msg.temp = std::vector<float>(begin(data->temperature), end(data->temperature));
    }
    status_msg.enabled = std::vector<bool>({enabled, enabled, enabled, enabled, enabled, enabled, enabled});
    // TODO: Fill this with real data
    status_msg.emergencystop = std::vector<bool>({false, false, false, false, false, false, false});
    status_msg.mode = 0;  // Always in joint position control
    status_pub->publish(status_msg);
    RCLCPP_DEBUG(this->get_logger(), "Sent status message!");
    kuka_debug_msg.step_duration = robot_status->get_step_duration();
    kuka_debug_msg.fri_frequency = robot_status->get_fri_frequency();
    RCLCPP_DEBUG(this->get_logger(), "Got frequencies!");
    kuka_debug_pub->publish(kuka_debug_msg);
    RCLCPP_DEBUG(this->get_logger(), "Finished status callback");
  }
  void cmd_callback(const impedance_control_msgs::msg::PosRef::SharedPtr msg) {
    RCLCPP_DEBUG(this->get_logger(), "Started cmd callback");
#ifdef MEASURE_TIME
    double current_time = this->now().seconds();
    double elapsed = current_time - prev_time;
    prev_time = current_time;
    counter++;
    frequencies[counter] = 1.0/elapsed;
    if (counter == 99) {
      double average = 0;
      for (int i=0; i<100; i++){
        average = average + frequencies[i] / 100;
      }
      fprintf(stderr, "Receiving commands at a frequency of %f Hz (average of 100 values)\r", average);
      counter = 0;
    }
#endif
    // Ignore incoming commands when the robot is not enabled
    if (enabled) {
      const std::lock_guard<std::mutex> lock(cmd->locker);
      for (int i=0; i<NUMBER_OF_JOINTS; i++){
        // Now we will accept position
        // TODO: change the Robot Command class to accept position instead
        cmd->command[i] = msg->position[i];
      }
    }
    RCLCPP_DEBUG(this->get_logger(), "Finished cmd callback");
  }
  void config_callback(const impedance_control_msgs::msg::RTParams::SharedPtr msg) const{
    bool erroneous_damping = false;
    bool erroneous_stiff = false;
    const std::lock_guard<std::mutex> lock(cmd->locker);
    for (int i=0; i<NUMBER_OF_JOINTS; i++){
      cmd->velocity[i] = msg->velocity[i];
      if (msg->damping[i] >= 0.0 && msg->damping[i] <= 1.0) {
        cmd->damping[i] = msg->damping[i];
      } else {erroneous_damping = true;}
      if (msg->stiffness[i] >= 0.0) {
        cmd->stiffness[i] = msg->stiffness[i];
      } else {erroneous_stiff = true;}
    }
    if (erroneous_damping) {
      RCLCPP_WARN(this->get_logger(), "Erroneous Damping data, ignored problematic values");
    }
    if (erroneous_stiff) {
      RCLCPP_WARN(this->get_logger(), "Erroneous Stiffness data, ignored problematic values");
    }
    RCLCPP_DEBUG(this->get_logger(), "Finished config callback");
  }
  void modes_callback(const std::shared_ptr<impedance_control_msgs::srv::Modes::Request> request, std::shared_ptr<impedance_control_msgs::srv::Modes::Response> response) {
    {
      const std::lock_guard<std::mutex> lock(robot_status->locker);
      hardware_enabled = robot_status->hardware_enabled;
    }
    if (request->mode != 0) {
      std::cout<<"Invalid control mode. Only Joint position control is allowed. Disabling robot!"<<std::endl;
      enabled = false;
      response->result="Invalid";
    }
    else {
      // If at least one joint is disabled, then disable the entire arm.
      enabled = true;
      for (int i=0; i<NUMBER_OF_JOINTS; i++) {
        enabled = enabled && request->enable[i];
      }
      if (!enabled) {
        std::cout<<"At least one joint was disabled. Disabling entire robot!"<<std::endl;
      }
      else {
        std::cout<<"Received a Software enable..."<<std::endl;
      }
      if (enabled && !hardware_enabled){
        response->result="Failure";
        std::cout<<"Arm is disabled by hardware, impossible to enable by software!"<<std::endl;
      }
      else {
        response->result="Success";
      }
    }
    {
      const std::lock_guard<std::mutex> lock(robot_status->locker);
      robot_status->software_enabled = enabled;
    }
    response->enabled = std::vector<bool>({
        enabled and hardware_enabled,
        enabled and hardware_enabled,
        enabled and hardware_enabled,
        enabled and hardware_enabled,
        enabled and hardware_enabled,
        enabled and hardware_enabled,
        enabled and hardware_enabled});
    response->mode=0;
  }
};


//! Ctrl-C handler
void catchsignal(int signo) {
  printf("\n Signal %d caught Ctrl-C, exiting...\n", signo);
  fri_interface->stop();

  fprintf(stderr, "Shutting down ROS...\n");
  rclcpp::shutdown();

  std::exit(EXIT_FAILURE);
}

//*******************************************************************************************
// main()
//
int main(int argc, char * argv[]) {
  // install Ctrl-C handler
  struct sigaction act;
  act.sa_handler = catchsignal;
  act.sa_flags = 0;
  if ((sigemptyset(&act.sa_mask) == -1) ||
      (sigaction(SIGINT, &act, NULL) == -1)) {
    printf("# Failed to set SIGINT to handle Ctrl-C, oh well\n");
  }
  auto data = new RobotData();
  auto cmd = new RobotCommand();
  auto status = new RobotStatus();

  rclcpp::init(argc, argv);
  auto fri_ros2_node = std::make_shared<FriNode>(data, cmd, status);
  if (!fri_ros2_node->configure_correct) {
    return 1;
  }
  fri_interface = new FriInterface(data,
                                   cmd,
                                   status,
                                   SERVER_PORT_NUMBER,
                                   fri_ros2_node->get_server_ip(),
                                   fri_ros2_node->max_velocity,
                                   fri_ros2_node->max_acceleration,
                                   fri_ros2_node->max_jerk);
  fri_interface->start();
  fprintf(stderr, "Initialized FRI interface\n");
  rclcpp::spin(fri_ros2_node);
  fprintf(stderr, "Ended FRI node\n");
  fri_interface->stop();
  rclcpp::shutdown();

  return (EXIT_SUCCESS);
}
