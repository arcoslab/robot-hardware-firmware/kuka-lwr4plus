// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <robot_status.hpp>

RobotStatus::RobotStatus() {
  hardware_enabled = true;
  software_enabled = false;
  drives_warning = false;
  fri_mode = FRI_STATE_MON;
  control_scheme = FastResearchInterface::JOINT_IMPEDANCE_CONTROL;
  communication_timing_quality = 0;
  control_loop_step_duration = 0.0;
  fri_frequency = 0.0;
}


void RobotStatus::from_robot(FastResearchInterface* robot){
  const std::lock_guard<std::mutex> lock_to(this->locker);
  hardware_enabled = robot->IsMachineOK();
  drives_warning = robot->DoesAnyDriveSignalAWarning();
  fri_mode = robot->GetFRIMode();
  control_scheme = robot->GetCurrentControlScheme();
  communication_timing_quality = robot->GetCommunicationTimingQuality();
}

void RobotStatus::set_frequencies(double step_duration, double fri_frequency) {
  const std::lock_guard<std::mutex> lock_this(this->locker);
  this->control_loop_step_duration = step_duration;
  this->fri_frequency = fri_frequency;
}

double RobotStatus::get_step_duration(){
  double result;
  {
    const std::lock_guard<std::mutex> lock_this(this->locker);
    result = this->control_loop_step_duration;
  }
  return result;
}

double RobotStatus::get_fri_frequency() {
  double result;
  {
    const std::lock_guard<std::mutex> lock_this(this->locker);
    result = this->fri_frequency;
  }
  return result;
}
