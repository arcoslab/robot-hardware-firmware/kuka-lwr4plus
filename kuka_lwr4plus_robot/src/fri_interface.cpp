// Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
// Universidad de Costa Rica
// Authors: Daniel Garcia Vaglio degv364@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <fri_interface.hpp>

using namespace std;

std::thread* handler;

FriInterface::FriInterface(RobotData* in_data,
                           RobotCommand* in_cmd,
                           RobotStatus* in_status,
                           int server_port_number,
                           unsigned int server_ip_address,
                           std::vector<double> max_velocity,
                           std::vector<double> max_acceleration,
                           std::vector<double> max_jerk) {
  keep_looping = true;
  connected = false;
  this->RML = new TypeIRML(NUMBER_OF_JOINTS, CYCLE_PERIOD);
  this->IP = new TypeIRMLInputParameters(NUMBER_OF_JOINTS);
  this->OP = new TypeIRMLOutputParameters(NUMBER_OF_JOINTS);

  safety_set_side(SAFETY_RIGHT);

  char robot_fri_driver_name[] = ROBOT_FRI_DRIVER_NAME;
  char logging_path[] = LOGGING_PATH;
  char logging_file_name[] = LOGGING_FILE_NAME;
  //this->Robot = new FastResearchInterface("/home/oms/local/src/fri-stanford/etc/980039-FRI-Driver.init");
  this->Robot = new FastResearchInterface(robot_fri_driver_name,
                                          KRC_COMMUNICATION_THREAD_PRIORITY,
                                          TIMER_THREAD_PRIORITY,
                                          MAIN_THREAD_PRIORITY,
                                          OUTPUT_CONSOLE_THREAD_PRIORITY,
                                          CYCLE_PERIOD,
                                          NUMBER_OF_LOGGING_FILE_ENTRIES,
                                          server_port_number,
                                          server_ip_address,
                                          logging_path,
                                          logging_file_name);

  this->external_data = in_data;
  this->external_cmd = in_cmd;
  this->robot_status = in_status;

  ruckig_input.synchronization=ruckig::Synchronization::None;
  // Target velocity should be usually zero. Unless multiple moving goals are desired
  ruckig_input.target_velocity = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  for (int i=0; i<NUMBER_OF_JOINTS; i++) {
    ruckig_input.max_velocity.at(i) = max_velocity.at(i);
    ruckig_input.max_acceleration.at(i) = max_acceleration.at(i);
    ruckig_input.max_jerk.at(i) = max_jerk.at(i);
  }

  for (int i=0; i < NUMBER_OF_JOINTS; i++) {
    cmd.velocity[i] = 0.5;
    cmd.stiffness[i] = 200.0;
    cmd.damping[i] = 0.7;
    cmd.addTorque[i] = 0.0;
  }

  Robot->SetCommandedJointStiffness(cmd.stiffness);
  Robot->SetCommandedJointDamping(cmd.damping);
  Robot->SetCommandedJointTorques(cmd.addTorque);
  this->internal_clock = rclcpp::Clock(RCL_SYSTEM_TIME);
  this->time_zero = rclcpp::Time(0, 0, RCL_SYSTEM_TIME);
  this->prev_time = time_zero;
  fprintf(stderr, "Created the Interface for KRC\n");
}

void loop(FriInterface* fri_interface){
  fri_interface->control_loop();
}

void FriInterface::start() {
  handler = new thread(loop, this);
}

void FriInterface::stop() {
  int result = EOK;
  keep_looping = false;
  if (handler->joinable()){
    handler->join();
  }
  if (Robot->IsMachineOK()) {
    result = Robot->StopRobot();
  }
  if (result != EOK) {
    fprintf(stderr, "An error occurred during stopping the robot...\n");
  }
  delete Robot;
}

void FriInterface::control_loop() {
  int return_value = 0;
  printf("Starting robot, this may take some time ...\n");
  // Retry to conenct until successful
  while (!connected) {
    // Set the initial parameters and do the first step of the
    // control loop. This is to send as first command, the current position
    return_value = Robot->StartRobot(FastResearchInterface::JOINT_IMPEDANCE_CONTROL, 120);
    if ( return_value != EOK) {
      Robot->printf("Could not start the robot, retrying...\n");
    }
    else {
      Robot->printf("Connection to the robot has been successful!\n");
      connected = true;
    }
  }

  fprintf(stderr, "Waiting for intial Tick\n");
  // Wait for KRC response
  Robot->WaitForKRCTick();
  if (!Robot->IsMachineOK()) {
    fprintf(stderr, "ERROR, the machine is not ready anymore.\n");
    return;
  }

  Robot->GetEstimatedExternalJointTorques(data.torque);
  Robot->GetMeasuredJointPositions(data.position);
  Robot->GetCommandedJointPositions(data.commanded);
  Robot->GetDriveTemperatures(data.temperature);

  std::cout<<"Initial commanded Joint position: "<<std::endl;
  // Set as first command the current position
  for (int i = 0; i < NUMBER_OF_JOINTS; i++) {
    // The first update must be the commanded position, and not the
    // current position. Depending on the stiffness there could be
    // an error between the commanded position and the current position
    // If we send the current position the robot will try to move.
    // The commanded position will avoid the movement.
    cmd.command[i] = data.commanded[i];
  }
  external_cmd->copy_from(&cmd);

  // Possibly enable movement?
  Robot->SetKRLBoolValue(0, true);
  Robot->SetKRLBoolValue(1, true);

  Robot->SetCommandedJointPositions(cmd.command);
  Robot->SetCommandedJointTorques(cmd.addTorque);
  Robot->SetCommandedJointStiffness(cmd.stiffness);
  Robot->SetCommandedJointDamping(cmd.damping);

  // Get the initial status from the robot
  robot_status->from_robot(Robot);

  fprintf(stderr, "Sent initial FRI instructions\n");
  while(keep_looping){
    return_value = this->step();
    if (return_value != 0) {
      // TODO: set robot status to disabled
      keep_looping = false;
      if (return_value == -1){
        // Something went terribly wrong, communication with the robot
        // is not possible or the timing was incorrect. For safety STOP
        // everything
        Robot->printf("ERROR\n");
        break;
      }
      else {
        // The robot is not Ready to receive commands, so we should check what is
        // going on.
        if (Robot->DoesAnyDriveSignalAnError()) {
          Robot->printf("ERROR: There is an error with a joint. Check the robot.\n");
          // This is a critical error, probably hardware related. STOP everything
          break;
        }
        if (!Robot->IsRobotArmPowerOn()){
          Robot->printf("ERROR: Robot Arm is Off, or the brakes are engaged.\n");
          // For now the only way to re-enable the robot from this state, is to do it
          // directly from hardware. There is no way to re-enable from software.
          // STOP everything
          break;
        }
        if (Robot->GetFRIMode() == FRI_STATE_MON) {
          Robot->printf("INFO: lost Command mode, trying to re-activate\n");
          return_value = Robot->StartRobot(FastResearchInterface::JOINT_IMPEDANCE_CONTROL, 120);
          keep_looping = true;  // We can keep retrying to start FRI
          if (return_value != EOK) {
            Robot->printf("ERROR, could not start robot: %s\n", strerror(return_value));
          }
        }
        else {
          Robot->printf("ERROR: Lost connection with the robot\n");
          // KRC is not even in Monitor mode, FRI was closed. STOP everything
        }
      }
    }
  }
  fprintf(stderr, "Closing FRI interface\n");
}

int FriInterface::step() {
  // Wait to get a datagram form the robotic arm. We are setting a timeout of
  // 2000 us (or 2ms) which will tell us that the KRC was unable to send a
  // datagram for an entire control cycle.
  int ret = Robot->WaitForKRCTick(2000);
  if (ret != EOK) {
    if (ret == EINVAL) {
      // There was an internal error
      Robot->printf("Internal error while waiting for KRC ticks");
      return -1;  // TODO: Define an error code policy
    } else if (ret == ETIMEDOUT) {
      // Did not receive a response in the specified time
      Robot->printf("Timeout while waiting for KRC ticks");
      return -1;  // TODO: define an error code policy
    } else {
      Robot->printf("Something went wrong while waiting for KRC ticks");
      return -1; // TODO: define an error code policy
    }
  }

  double current_frequency = 100.0;
  auto start_time = this->internal_clock.now();
  if (this->prev_time != this->time_zero) {
    auto period = start_time - prev_time;
    current_frequency = 1.0 / period.seconds();
  }
  prev_time = start_time;


  // Get current robot state
  Robot->GetEstimatedExternalJointTorques(data.torque);
  Robot->GetMeasuredJointPositions(data.position);
  Robot->GetDriveTemperatures(data.temperature);
  robot_status->from_robot(Robot);

  // Up to this point it doesn't matter if the robot is in COMMAND
  // or MONITOR mode. Before sending commands, we should make sure the
  // robot is able to execute them.
  if (!Robot->IsMachineOK()) {
    // Send the robot state to  the rest of threads
    external_data->copy_from(&data);
    return 1;
  }

  if (!robot_status->software_enabled) {
    // The robot was been disabled by software, so we just ignore all the
    // commands without error.
    // Resend the last command continually to make sure the robot stops
    // movement but not other functions
    external_data->copy_from(&data);
    Robot->SetCommandedJointPositions(data.commanded);
    Robot->SetCommandedJointTorques(cmd.addTorque);
    Robot->SetCommandedJointStiffness(cmd.stiffness);
    Robot->SetCommandedJointDamping(cmd.damping);
    return 0;
  }

  // Receive the command from an external thread (from ROS)
  cmd.copy_from(external_cmd);

  // Ruckig target position from new ROS command
  for (int j=0; j< NUMBER_OF_JOINTS; j++) {
    ruckig_input.current_position[j]=data.commanded[j];
    ruckig_input.target_position[j]=cmd.command[j];
  }
  
  // ruckig: calculation
  result = my_ruckig.update(ruckig_input, ruckig_output);

  // TODO: Mutexes here?
  for (int j=0; j< NUMBER_OF_JOINTS; j++) {
    cmd.command[j]=ruckig_output.new_position[j];
    if (cmd.command[j] >= limh[j]) {
      Robot->printf("Joint %d Reaching Software limit!\n", j);
      cmd.command[j] = limh[j];
    }
    if (cmd.command[j] <= liml[j]) {
      Robot->printf("Joint %d Reaching Software limit!\n", j);
      cmd.command[j] = liml[j];
    }
    data.commanded[j] = cmd.command[j];
  }

  // ruckig: Updating input from current output
  if (result == ruckig::Result::Working) {
    ruckig_output.pass_to_input(ruckig_input);
  }

  // Send the robot state to the rest of threads
  external_data->copy_from(&data);

  // Send the command to the robot
  Robot->SetCommandedJointPositions(cmd.command);
  Robot->SetCommandedJointTorques(cmd.addTorque);
  Robot->SetCommandedJointStiffness(cmd.stiffness);
  Robot->SetCommandedJointDamping(cmd.damping);

  // Step duration average
  auto step_duration = this->internal_clock.now() - start_time;
  this->step_durations.push_back(step_duration.seconds());
  if (this->step_durations.size() > 100) {
    this->step_durations.pop_front();
  }
  int cant = step_durations.size();
  double step_average = 0;
  for (auto val=this->step_durations.begin();
       val !=this->step_durations.end();
       val++) {
    step_average = step_average + (*val) / ((double) cant);
  }

  // Frequency average
  this->fri_frequencies.push_back(current_frequency);
  if (this->fri_frequencies.size() > 100){
    this->fri_frequencies.pop_front();
  }
  cant = fri_frequencies.size();
  double average = 0;
  for (auto val=this->fri_frequencies.begin();
       val != this->fri_frequencies.end();
       val++){
    average = average + (*val) / ((double) cant);
  }
  robot_status->set_frequencies(step_average, average);

  return 0;
}
