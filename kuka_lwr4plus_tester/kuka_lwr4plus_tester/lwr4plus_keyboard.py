# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import click
import yaml
import numpy as np
import rclpy
from threading import Thread
from rclpy.node import Node

from impedance_control_msgs.msg import State, PosRef, RTParams


class FriTester(Node):

    def __init__(self):
        self.got_first_position = False
        self.control_variable = 8  # By default control stiffness
        self.count = 0
        self.user_alerts = ""
        self.recorded_joint_positions = []

        super(FriTester, self).__init__('fri_keyboard')

        self.declare_parameter("side", "right")
        t_base = "kuka_lwr4plus_" + str(self.get_parameter("side").value) + "/"
        self.cmd_pub = self.create_publisher(
            PosRef, t_base + "posref", 10)
        self.config_pub = self.create_publisher(
            RTParams, t_base + "rtparams", 10)
        self.state_sub = self.create_subscription(
            State, t_base + "state", self.state_callback, 10)

        timer_period = 1/20  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.angle_values = np.array([0.0]*7)
        self.position = np.array([0.0]*7)
        self.effort = np.array([0.0]*7)
        self.commanded = np.array([0.0]*7)

        self.increment = 3 * np.pi/180  # 3 degrees for fine adjustment
        self.stiffness = np.array([100.0]*7)
        self.damping = np.array([0.7]*7)

        self.usage_message()

    def state_callback(self, msg):
        self.position = np.array(list(msg.position))
        self.effort = np.array(list(msg.effort))
        self.commanded = np.array(list(msg.cmdedposition))
        if not self.got_first_position:
            # The first angle values to use is the current commanded position
            self.angle_values = self.commanded
            self.got_first_position = True

    def timer_callback(self):
        # self.get_keyboard()

        config = RTParams()
        config.stiffness = list(self.stiffness)
        config.damping = list(self.damping)
        config.velocity = [0.5]*7

        cmd = PosRef()
        cmd.position = list(self.angle_values)
        if self.got_first_position:
            self.cmd_pub.publish(cmd)
            self.config_pub.publish(config)
        self.count += 1
        if self.count == 5:
            self.count = 0
            self.usage_message()

    def get_keyboard(self):
        char = click.getchar()

        if char in "1234567":
            self.control_variable = int(char)
        if char == "s" or char == "S":
            # control stiffness
            self.control_variable = 8
        if char == "d" or char == "D":
            # control damping
            self.control_variable = 9

        if char == "a" or char == "A":
            new_list = [float(value) for value in self.commanded]
            self.recorded_joint_positions.append(new_list)
            print("Recorded position...\n\n")

        if char == "f" or char == "F":
            dic = {
                "side": self.get_parameter("side").value,
                "positions": self.recorded_joint_positions
            }
            with open("output.yaml", "w") as output_file:
                yaml.safe_dump(
                    dic, stream=output_file)

        if 0 < self.control_variable < 8:
            # Controlling joints
            if char == "\x1b[A":  # Arrow up
                self.angle_values[self.control_variable-1] += self.increment
            if char == "\x1b[B":  # Arrow down
                self.angle_values[self.control_variable-1] -= self.increment
        elif self.control_variable == 8:
            # Controlling stiffness
            if char == "\x1b[A":  # Arrow up
                self.stiffness += np.array([5]*7)
            if char == "\x1b[B":  # Arrow down
                self.stiffness -= np.array([5]*7)
        elif self.control_variable == 9:
            # Controlling dampness
            if char == "\x1b[A":  # Arrow up
                self.damping += np.array([0.1]*7)
            if char == "\x1b[B":  # Arrow down
                self.damping -= np.array([0.1]*7)
        else:
            print("No var")

        self._enforce_limits()

    def _enforce_limits(self):
        damping_alert = ""
        stiffness_alert = ""
        if self.damping[0] < 0.1:
            self.damping = np.array([0.1]*7)
            damping_alert = "Truncating damping to a minimum of 0.1"
        elif self.damping[0] > 0.9:
            self.damping = np.array([0.9]*7)
            damping_alert = "Truncating damping to a maximum of 0.9"
        else:
            damping_alert = ""

        if self.stiffness[0] < 10:
            self.stiffness = np.array([10.0]*7)
            stiffness_alert = "Truncating stiffness to a minimum of 10"
        elif self.stiffness[0] > 500:
            self.stiffness = np.array([500.0]*7)
            stiffness_alert = "Truncating stiffness to a maximum of 500"
        else:
            stiffness_alert = ""

        self.user_alerts = damping_alert + " " + stiffness_alert

    def add_value(self, value_index, name, values):
        if self.control_variable == value_index:
            color = "green"
            front = "→ "
        else:
            color = "white"
            front = "  "
        msg = "".join([
            front,
            name,
            ": "])
        for value in values:
            msg += "{:.5f}".format(value) + " "
        return click.style(msg, fg=color)

    def usage_message(self):
        click.clear()
        msg = []
        msg.append("="*80)
        msg.append("Select a joint to control by pressing the number (1-7)")
        msg.append("Press 'S' to control stiffness")
        msg.append("Press 'D' to control damping")
        msg.append("Use arrow keys ↑ ↓ to increase or decrease the value")
        msg.append("")
        msg.append("Press 'A' to add the current cmded joint pos to the storage_list")
        msg.append("Press 'F' to store the positions list to a file")
        msg.append("")
        msg.append("Current state:")
        msg.append("Joint-cmd-----pos---")
        for joint_idx in range(7):
            msg.append(
                self.add_value(
                    joint_idx+1,
                    str(joint_idx+1),
                    [self.angle_values[joint_idx], self.position[joint_idx]]))
        msg.append("")
        msg.append(self.add_value(8, "Stiffness", [self.stiffness[0]]))
        msg.append(self.add_value(9, "Damping", [self.damping[0]]))
        msg.append("")
        if self.got_first_position:
            msg.append(click.style("Got first position, commands enabled!", fg="blue"))
        else:
            msg.append(click.style("Waiting first position, commands disabled", fg="red"))

        msg.append(click.style(self.user_alerts, bg="yellow"))
        msg.append("="*80)

        click.echo("\n\r".join(msg))


def keyboard_loop(node):
    # This will block until it gets a new key
    while True:
        node.get_keyboard()


def main(args=None):
    rclpy.init(args=args)

    fri_tester = FriTester()

    keyboard_listener = Thread(target=keyboard_loop, args=[fri_tester])
    keyboard_listener.start()
    rclpy.spin(fri_tester)

    fri_tester.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
