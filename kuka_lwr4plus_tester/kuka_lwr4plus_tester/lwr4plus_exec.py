# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import yaml
import click
import rclpy
from numpy import pi
from rclpy.node import Node
from impedance_control_msgs.msg import PosRef, RTParams, State


def is_close(left, right, eps=2*pi/180):
    """
    Calculate if two positions are close one to the other
    """
    assert len(left) == len(right)
    close = True
    for idx in range(len(left)):
        distance = abs(left[idx] - right[idx])
        if distance > eps:
            close = False
    return close


class FriTester(Node):
    def __init__(self):
        super().__init__("fri_tester")

        # State variables
        self.got_first_position = False
        self.current_index = 0
        self.waiting_for_keyboard = False

        # Get side and configure topic names
        self.declare_parameter("side", "right")
        t_base = "kuka_lwr4plus_" + str(self.get_parameter("side").value) + "/"

        # Get the positions to execute from the yaml file
        self.declare_parameter("file_name", "positions.yaml")
        with open(self.get_parameter("file_name").value) as positions_file:
            positions_dict = yaml.safe_load(positions_file)
        assert positions_dict["side"] == str(self.get_parameter("side").value)
        self.position_list = positions_dict["positions"]

        # Do we wait for keyboard?
        self.declare_parameter("wait_keyboard", False)
        self.wait_keyboard = bool(self.get_parameter("wait_keyboard").value)

        # ROS2 stuff
        self.cmd_pub = self.create_publisher(
            PosRef, t_base + "posref",
            10)
        self.config_pub = self.create_publisher(
            RTParams, t_base + "rtparams",
            10)
        self.sub_state = self.create_subscription(
            State, t_base + "state",
            self.state_callback, 10)
        self.cmd_timer = self.create_timer(1/60, self.cmd_callback)
        self.config_timer = self.create_timer(1, self.config_callback)

    def state_callback(self, msg):
        self.current_position = list(msg.position)
        self.current_commanded = list(msg.cmdedposition)
        self.got_first_position = True

    def config_callback(self):
        config = RTParams()
        config.velocity = [0.0]*7
        config.stiffness = [100.0]*7
        config.damping = [0.8]*7
        self.config_pub.publish(config)

    def cmd_callback(self):
        if self.got_first_position:
            self.execute_callback()

    def execute_callback(self):
        if self.waiting_for_keyboard:
            return
        current_goal = self.position_list[self.current_index]
        if is_close(self.current_position, current_goal):
            click.secho(
                "Reached position "+str(self.current_index),
                fg="green")
            self.current_index += 1
            if self.wait_keyboard:
                print("press keyboard")
                self.waiting_for_keyboard = True
                click.getchar()
                self.waiting_for_keyboard = False
            if self.current_index == len(self.position_list):
                click.secho("Finished Execution!", fg="green")
                raise SystemExit

        cmd = PosRef()
        cmd.position = current_goal
        self.cmd_pub.publish(cmd)


def main(args=None):
    rclpy.init(args=args)
    fri_tester = FriTester()
    try:
        rclpy.spin(fri_tester)
    except SystemExit:
        rclpy.logging.get_logger("Quitting").info('Done')

    fri_tester.destroy_node()
    rclpy.shutdown()
