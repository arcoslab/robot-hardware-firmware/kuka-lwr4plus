# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import click
import json
import sys
import rclpy
from threading import Thread
from impedance_control_msgs.msg import State, RTParams, PosRef
from rclpy.node import Node


class FriTester(Node):

    def __init__(self):
        super(FriTester, self).__init__('fri_keyboard')
        self.declare_parameter("side", "right")
        t_base = "kuka_lwr4plus_" + str(self.get_parameter("side").value) + "/"

        self.declare_parameter("file_name", "positions.json")
        self.got_first_position = False
        self.position = [0.0]*7
        self.commanded = [0.0]*7

        self.stored_positions = []

        self.state_sub = self.create_subscription(
            State, t_base + "state", self.state_callback, 10)
        self.config_pub = self.create_publisher(
            RTParams, t_base + "rtparams", 10)
        self.cmd_pub = self.create_publisher(
            PosRef, t_base + "posref", 10)

        self.cmd_timer = self.create_timer(1/300, self.cmd_callback)
        self.config_timer = self.create_timer(1/60, self.config_callback)

    def state_callback(self, msg):
        self.position = msg.position
        self.commanded = msg.cmdedposition
        if not self.got_first_position:
            self.cmd = self.commanded
            self.got_first_position = True

    def cmd_callback(self):
        cmd = PosRef()
        if self.got_first_position:
            # The arm will go into gravity compensation
            cmd.position = self.position
            self.cmd_pub.publish(cmd)

    def config_callback(self):
        params = RTParams()
        params.stiffness = [200.0]*7
        params.damping = [0.5]*7
        params.velocity = [0.5]*7

        self.config_pub.publish(params)

    def get_keyboard(self):
        print("\n\r Press S to store the current position")
        print("\n\r Press Q to exit and save positions into a file")
        char = click.getchar()
        if char == "s" or char == "S":
            self.stored_positions.append(self.commanded)
            print("\n\r", len(self.stored_positions),
                  " Positions have been stored")
            print("\n\r New position:", list(self.commanded))
        if char == "q" or char == "Q":
            print("\n\r Quit. Saving positions into positions.json")
            positions_json = json.dumps({
                "side": str(self.get_parameter("side").value),
                "positions": [list(pos) for pos in self.stored_positions]})
            print(positions_json)
            positions_file = open(
                str(self.get_parameter("file_name").value), "w")
            positions_file.write(positions_json)
            positions_file.close()
            sys.exit()


def keyboard_loop(node):
    # Block until a new key
    while True:
        node.get_keyboard()


def main(args=None):
    rclpy.init(args=args)

    fri_tester = FriTester()

    keyboard_listener = Thread(target=keyboard_loop, args=[fri_tester])
    keyboard_listener.start()
    rclpy.spin(fri_tester)

    fri_tester.destroy_node()
    rclpy.shutdown()
