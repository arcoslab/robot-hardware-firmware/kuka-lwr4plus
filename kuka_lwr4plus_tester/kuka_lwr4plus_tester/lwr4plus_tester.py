# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import rclpy
from rclpy.node import Node

from impedance_control_msgs.msg import PosRef, RTParams, State


def _truncate(array, value):
    for idx in range(7):
        if abs(array[idx]) > value:
            array[idx] = np.sign(array[idx]) * value
    return array


def is_close(pos, goal, threshold=0.1):
    return np.linalg.norm(goal-pos) < threshold


def next_pos(current, cmded, goal, max_joint_speed, period):
    '''
    current: the current position
    cmded: the current commanded position
    goal: the goal to which we move
    max_joint_speed: max joint speed of any joint
    period: period at which we move
    '''
    if not is_close(current, cmded):
        # If the error between the cmded and the position is
        # too high, something is applying force. Keep the
        # cmded position until it is reached
        return cmded
    max_delta = max_joint_speed * period
    diff = goal - cmded
    return cmded + _truncate(diff, max_delta)


class FriTester(Node):

    def __init__(self, position_list):
        super().__init__('fri_tester')

        self.declare_parameter("side", "right")
        t_base = "kuka_lwr4plus_" + str(self.get_parameter("side").value) + "/"

        # The first position in the list is the cmdedposition at the start
        # of communication
        self._position_list = [np.array([0.0]*7)] + position_list
        self.got_first_position = False
        self.cmdedposition = np.array([0.0]*7)
        self.current_position = np.array([0.0]*7)
        self._index = 0  # Current position being executed
        self.finished = False

        # ROS2 stuff
        self.cmd_pub = self.create_publisher(
            PosRef, t_base + "posref",
            10)
        self.config_pub = self.create_publisher(
            RTParams, t_base + "rtparams",
            10)
        self.current_state = self.create_subscription(
            State, t_base + "state",
            self.state_callback, 10)

        timer_period = 1/400  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def generate_command(self):
        if self._index < len(self._position_list):
            result = next_pos(
                current=self.current_position,
                cmded=self.cmdedposition,
                goal=self._position_list[self._index],
                max_joint_speed=0.9,
                period=1)
            if is_close(self.current_position,
                        self._position_list[self._index]):
                print("Completed Goal: ",list(self._position_list[self._index]))
                self._index += 1
            return result
        else:
            if not self.finished:
                print("Finished execution")
                self.finished = True
                raise SystemExit
            return self.cmdedposition

    def timer_callback(self):
        # Send the real time parameters
        config = RTParams()
        config.velocity = [0.0]*7
        config.stiffness = [100.0]*7
        config.damping = [0.4]*7
        self.config_pub.publish(config)

        # Create the command
        cmd = PosRef()
        if self.got_first_position:
            command = self.generate_command()
            # It is weird that suddenly ROS requires explicit casting
            cmd.position = [float(val) for val in command]
            self.cmd_pub.publish(cmd)
        else:
            cmd.position = list(self.cmdedposition)

    def state_callback(self, state_msg):
        self.current_position = np.array(state_msg.position)
        self.cmdedposition = np.array(state_msg.cmdedposition)
        self._position_list[0] = self.cmdedposition
        self.got_first_position = True


def main(args=None):
    rclpy.init(args=args)
    dar_la_mano = np.array(
        [1.34714, 1.23175, -0.33386, -1.33270, 2.08475, 1.35592, 0.69999])
    transition = np.array(
        [1.27214, 1.23175, -0.33386, 0.51730, 1.77975, 1.36592, 0.69999])
    wave_01 = np.array(
        [1.27214, 1.60925, -0.33386, 1.03730, 1.77975, 1.36592, 0.69999])
    wave_02 = np.array(
        [1.27214, 1.60925, -1.17136, 1.03730, 1.77975, 1.36592, 0.69999])

    position_list = [
        dar_la_mano,
        transition,
        wave_01,
        wave_02,
        wave_01,
        wave_02,
        wave_01,
        wave_02,
        wave_01,
        wave_02,
        wave_01,
        transition,
        dar_la_mano]  # They must be np-arrays

    fri_tester = FriTester(position_list)

    try:
        rclpy.spin(fri_tester)
    except SystemExit:
        rclpy.logging.get_logger("Quitting").info('Done')

    fri_tester.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
