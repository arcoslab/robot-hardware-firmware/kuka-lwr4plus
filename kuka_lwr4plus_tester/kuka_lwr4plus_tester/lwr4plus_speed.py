# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import rclpy
from rclpy.node import Node

from impedance_control_msgs.msg import PosRef, State


class FriTester(Node):

    def __init__(self):
        super().__init__('fri_speed')
        self.declare_parameter("side", "right")
        t_base = "kuka_lwr4plus_" + str(self.get_parameter("side").value) + "/"

        self.cmd_pub = self.create_publisher(
            PosRef, t_base + "posref",
            10)

        self.current_state = self.create_subscription(
            State, t_base + "state",
            self.state_callback, 10)

        self.declare_parameter("timer_period", 1.0/500.0);
        timer_period = float(self.get_parameter("timer_period").value)
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.got_first_position = False

        print("Generating messages at ", 1/timer_period, "Hz")

    def timer_callback(self):
        # Create the command
        cmd = PosRef()
        if self.got_first_position:
            cmd.position = self.cmdedposition

        self.cmd_pub.publish(cmd)

    def state_callback(self, state_msg):
        self.got_first_position = True
        self.cmdedposition = state_msg.cmdedposition


def main(args=None):
    rclpy.init(args=args)
    fri_tester = FriTester()
    rclpy.spin(fri_tester)
    fri_tester.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
