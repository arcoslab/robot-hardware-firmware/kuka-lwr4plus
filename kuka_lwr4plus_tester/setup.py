from setuptools import setup

package_name = 'kuka_lwr4plus_tester'

setup(
    name=package_name,
    version='0.3.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    author='Daniel Garcia-Vaglio',
    author_email='degv364@gmail.com',
    maintainer='Daniel Garcia-Vaglio',
    maintainer_email='degv364@gmail.com',
    keywords=['ROS'],
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Software Development',
    ],
    description='Minimal node for testing FRI interface for ROS2',
    license='General Public License 3.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'lwr4plus_tester = kuka_lwr4plus_tester.lwr4plus_tester:main',
            'lwr4plus_keyboard = kuka_lwr4plus_tester.lwr4plus_keyboard:main',
            'lwr4plus_cartesian = kuka_lwr4plus_tester.lwr4plus_cartesian:main',
            'lwr4plus_speed = kuka_lwr4plus_tester.lwr4plus_speed:main',
            'lwr4plus_pos_recorder = kuka_lwr4plus_tester.lwr4plus_pos_recorder:main',
            'lwr4plus_exec = kuka_lwr4plus_tester.lwr4plus_exec:main'
        ],
    },
)
